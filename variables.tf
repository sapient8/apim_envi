variable "api_name" {
  description = "Name of the API"
  type        = string
}

variable "resource_group_name" {
  description = "resource group of APIM"
  type        = string
}

variable "apim_name" {
  description = "Name of the APIM"
  type        = string
  
}

variable "revision" {
  description = "revision of API"
  type        = string
  
}

variable "display_api_name" {
  description = "display_name of API to be created"
  type        = string
  
}

variable "path" {
  description = "path of API"
  type        = string
  
}

variable "protocols" {
  description = "protocols  of API"
  type        = string
  
}



variable "Json_file" {
  description = "Json file from developer"
  type        = string
  
}

variable "apim_tag" {
  description = "Tag of the APIM"
  type        = string
  
}








