terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 3.0"
    }
  }

  required_version = ">= 1.2.4 "
  backend "azurerm" {
        resource_group_name  = "oss_dev"
        storage_account_name = "osstfstatedev1" 
        container_name       = "osstfstatedevcontainer1"
        key                  = "API/terraform.tfstate"
        use_microsoft_graph  = "false"
    } 

  
}

provider "azurerm" {
 
  features {}
}


