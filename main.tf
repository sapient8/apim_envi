resource "azurerm_api_management_api" "example" {
  name                = var.api_name
  resource_group_name = var.resource_group_name
  api_management_name = var.apim_name
  revision            = var.revision
  display_name        = var.display_api_name
  path                = var.path
  protocols           = [var.protocols]
  

  

import {
    content_format = "swagger-json"
    content_value  = file("${path.module}/${var.Json_file}")
  }
}
